=head1 NAME

downloads last.fm profiles and helps scrobbling from files or radio stations

=head1 DESCRIPTION

dl_lfm has several functions, it

* downloads a last.fm profile

* helps scrobbling from json files, radio streaming stations (using vlc) or some 
webpages (supported: bandcamp.com and experimentally youtube.com)

* plays and scrobbles randomly picked tracks from bandcamp.com like a radio 
(needs mplayer installed)

=head1 REQUIREMENTS FOR INSTALLATION

in debian, ubuntu, ... it should be enough to install the following packages:
sudo apt-get install libterm-readkey-perl libmp3-tag-perl libyaml-perl libjson-perl mplayer

=head1 SYNOPSIS

dl_lfm [options]

      --dl                    download all data from user (requires -u)
      --keep                  when using bandcamp radio, keep downloaded files 
                               (default: delete after playing)
      --password=string       password
      --password-file=string  a file containing your last-fm password
  -r  --radio=src             use radio station to play some tracks; if --user is 
                               set, then tracks will be scrobbled, see --man for 
                               suported urls; needs mplayer for bandcamp.
  -s  --scrobble=src          use this file or radio station to scrobble some tracks
                               without playing them, see --man for suported file 
                               types and urls
      --stream-out=dest       use this destination to send the stream to 
                               (if scrobbling from stream), default = '| vlc -'
  -t, --test                  don't scrobble anything, simulate only
      --timestamp=string      set timestamp (UTC) of album end, when scrobbling from 
                               bandcamp.com or yaml file; treat string as UTC timestamp string
      --timestamp-local=str   same as --timestamp, but treat str as local timestamp
                               (not UTC)
  -u, --user=string           username

meta:

  -V, --version               display version and exit.
  -h, --help                  display brief help
      --man                   display long help (man page)
  -q, --silent                same as --verbose=0
  -v, --verbose               same as --verbose=1 (default)
  -vv,--very-verbose          same as --verbose=2
  -v, --verbose=x             grade of verbosity
                               x=0: no output
                               x=1: default output
                               x=2: much output

example:

  dl_lfm --dl --user='testtesttest123123123'

    gets all public information on user 'testtesttest123123123' from last.fm 
    and saves that data into several files.

  dl_lfm --scrobble='https://someincredibleband.bandcamp/album/someawsomealbum' 
  --user="testtest123123"

    scrobbles given album to last.fm profile of user 'testtest12123'. the 
    scrobbling timestamps will be set as if you had just now finished listening to
    the album. tracks will not be played.

  dl_lfm --scrobble='https://someincredibleband.bandcamp/album/someawsomealbum' 
  --user="testtest123123" --timestamp='2016-01-01 12:34'

    same as above but sets the scrobbling timestamp as if you had finished 
    listening to the album at 2016-01-01 12:34 UTC.

  dl_lfm --radio='https://bandcamp.com/tag/progressivepostfuneraldoommetal' 

    plays random tracks of given tags from bandcamp.com. uses mplayer for playing.

  dl_lfm --radio='https://bandcamp.com/tag/progressivepostfuneraldoommetal' 
  --user="cutelittlesquirrel"

    plays random tracks of given tags from bandcamp.com and scrobbles them to 
    last.fm profile of user cutelittlesquirrel.

=head1 OPTIONS

=over 8

=item B<--dl>

requires B<--user>=I<username>.

gets all public information on I<username> from last.fm and saves that data into 
several files.
be aware that those can get quite big. a scrobble is approximately 2KiB large. 
so 10k scrobbles cost around 20MiB free space on your hard drive.

=item B<--password>=I<string>

use this password for last.fm user B<--user>=I<username>.

=item B<--password-file>=I<string>

use the first line of this file a password for last.fm. 
default = $HOME/.password_lastfm

=item B<--radio>=I<source>, B<-r> I<source>

use I<source> for playing some tracks. if B<--user>=I<username> is set, then the 
played tracks will be scrobbled to last.fm.

supported urls or radio streaming stations: 

* https://{band or label name}.bandcamp.com/album/{albumname}

* https://bandcamp.com/tag/{tagname}

* {protocol}://{ip address}:{port}

=item B<--scrobble>=I<source>, B<-s> I<source>

same as B<--radio>, but without playing the tracks (if possible).

requires B<--user>=I<username> for last.fm.

use I<source> for scrobbling some tracks to last.fm.

supported file type(s): json

OR

supported urls or radio streaming stations: 

* https://{band or label name}.bandcamp.com/album/{albumname}

* https://bandcamp.com/tag/{tagname}

* {protocol}://{ip address}:{port}

=item B<--stream-out>=I<destination>

if playing a radio stream (see B<--radio>), then this param can be used to adjust
the destination where the stream data shall forwarded to.

default: I<destination> = '| vlc -', i.e., the stream data will be piped '|' to a 
new instance of vlc, where the parameter '-' means, that vlc will read the stream 
from STDIN.

other possibilities: '> output.mp3' to pipe the streaming data directly into a file.

=item B<--user>=I<username>, B<-u> I<username>

use this last.fm username, e.g. for B<--dl>

=item B<--test>, B<-t>

don't scrobble anything, but simulate only

=item B<--version>, B<-V>

prints version and exits.

=item B<--help>, B<-h>, B<-?>

prints a brief help message and exits.

=item B<--man>

prints the manual page and exits.

=item B<--verbose>=I<number>, B<-v> I<number>

set grade of verbosity to I<number>. if I<number>==0 then no output
will be given, except hard errors. the higher I<number> is, the more 
output will be printed. default: I<number> = 1.

=item B<--silent, --quiet, -q>

same as B<--verbose=0>.

=item B<--very-verbose, -vv>

same as B<--verbose=3>. you may use B<-vvv> for B<--verbose=4> a.s.o.

=item B<--verbose, -v>

same as B<--verbose=2>.

=back

=head1 LICENCE

Copyright (c) 2017, seth
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

originally written by seth

